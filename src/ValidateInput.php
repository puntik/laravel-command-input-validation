<?php declare(strict_types = 1);

namespace Abetzi\ValidateInput;

use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

trait ValidateInput
{

    /** @var Validator */
    protected $validator;

    /** @var array */
    protected $validated = [];

    /** @return array */
    abstract protected function rules(): array;

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        throw_if(
            $this->validator()->fails(),
            new InvalidArgumentException(implode(PHP_EOL, $this->validator()->errors()->all()))
        );

        $this->validated = $this->validator()->validate();

        return parent::execute($input, $output);
    }

    protected function validator()
    {
        if (isset($this->validator)) {
            return $this->validator;
        }

        $data = array_filter(array_merge($this->arguments(), $this->options()), function ($value) {
            return $value !== null;
        });

        return $this->validator = Validator::make(
            $data,
            $this->rules()
        );
    }
}

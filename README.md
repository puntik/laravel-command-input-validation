# Validate command input

Simple laravel package to validate input in Artisan console command inputs.

## Install

```shell
composer require abetzi/validate-input
```

## Example

~~~php
<?php

use Abetzi\ValidateInput\ValidateInput;

class ValidationOptionsCommand extends Command
{

    use ValidateInput;

    protected $signature = 'zend:opt
        {name}
        {--country=cs}
        {--user=}
        {--admin}
    ';

    protected $description = 'Command description';

    public function handle()
    {
        // do something useful
        dump($this->validated);
        
        return 0;
    }

    protected function rules(): array
    {
        return $rules = [
            'name'    => 'required',
            'country' => 'required|in:cs,sk,pl,hu',
            'user'    => 'required|exists:users,id',
            'admin'   => 'boolean',
        ];
    }
}
~~~
